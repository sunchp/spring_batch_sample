package com.sunchp.springbatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

/**
 * Created by sunchangpeng.
 */
public class MessagesItemWriter implements ItemWriter<Message> {
    private static final Logger logger = LoggerFactory.getLogger(MessagesItemWriter.class);

    public void write(List<? extends Message> messages) throws Exception {
        logger.info("write results");

        for (Message m : messages) {
            logger.info(m.getContent());
        }
    }
}
