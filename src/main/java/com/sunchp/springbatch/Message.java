package com.sunchp.springbatch;

/**
 * Created by sunchangpeng.
 */
public class Message {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
